ServiceDescriptor Module
=======================================

.. automodule:: SiLA2CodeGenPackage.service_descriptor_parser

ServiceDescriptor Class
---------------------------------------

.. autoclass:: SiLA2CodeGenPackage.service_descriptor_parser.ServiceDescriptorParser
    :members:
    :private-members:
    :special-members:
    :exclude-members: __weakref__
