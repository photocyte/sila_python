# ObservableProperty

This folder contains an example project with an observable property.
Using the SiLACodeGenerator the FDL file can be converted into a working example of a server and client implementation.

## Final: ObservableProperty

The sub-directory `final` contains one of these generated implementations with a few manual modifications:

 * The observable property has been implemented in the simulation mode implementation to return random integers between 0 and 100 *as long as* the property value has not been set explicitly. 
 * The client does not execute the `SetProperty` command
 * The property is read in observable manner in a loop for ~ 10 s. After approximately 5 seconds it is manually set to 42 using the `SetProperty` command. The value of the property is printed to the terminal.
 
## Notes on gRPC Streams and Observable Properties

The way streams are implemented in gRPC makes the server cache an undefined number of values, i.e. a client that pauses (e.g. `time.sleep(1.0)`) during reading the values will **always** receive old values. It should thus be ensured that values are read as fast as possible. 
I personally would also recommend to implement a small delay on the server side to slow down the generation of values, so that the client has a realistic chance to keep up.

This can easily be visualised if you uncomment the `time.sleep(0.2)` line in the `_simulation` implementation. Executing the client now will not yield any value 42 (well.. not on my computer), though it should be there after ~ 5 s.