# Examples

Directory of example projects that can be compiled to server/client prototypes. Assuming the `SiLA2CodeGenerator` package is installed, this can be achieved by running

```bash
silacodegenerator --build <project_directory>
```

## Note

The examples in this directory will be synced via git, if, and only if, their directory name ends with *_project*. Please respect this naming scheme when adding further examples, and furthermore make sure that the resulting project (service name) does **not** end in *_project*

This ensures that compiled projects are not synced to the server.