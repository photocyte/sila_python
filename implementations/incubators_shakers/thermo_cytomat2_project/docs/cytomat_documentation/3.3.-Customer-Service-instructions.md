**3.3. Customer-Service-Instructions**

*  With the [Customer-Service-Instructions](3.3 Customer-Service-Instructions) it is possible to configure the [PSS](1. Terms) and enable the communication with:
    *  RS232-Interface and [Barcode-Scanner](1. Terms)
    *  RS232-Interface and [Heating-System](1. Terms) / [CO2-Feed](1. Terms)

| Command | Response | Meaning | Test
| ------ | ------ | ------ | ------ |
| `se:ns` | `ok**` (if command accepted) or `er**` ([If command declined](2.2 Error-Codes)) | Restart the System<br>Conditions: none | 02.04.2019 TL: working |
| `se:cs xxx yyy` | `ok**` or `er**` | configure [Stacker](1. Terms) XXX with [Pitch](1. Terms)-Value yyy<br>**Conditions:** none | - |
| `se:c1` | `ok**` or `er**` | Read and output [Barcode-Scanner](1. Terms) data via the RS 232 Interface<br>**Conditions:** [Barcode-Scanner](1. Terms) is installed  | 01.04.2019: no reaction (no er** or ok**) from machine |
| `se:c2` |  `ok**` or `er**` | Read and output [Heating-System](1. Terms) and [CO2-Feed](1. Terms) Data via the RS 232 Interface<br>**Conditions:** none.| 01.04.2019: no reaction (no er** or ok**) from machine |

**Example**

![image](uploads/b6e7e787ec5a615be7442a833ae9c001/image.png)

*  A 23mm [Pitch](1. Terms)-[Stacker](1. Terms) is configured on slot 1 of the [Baseplate](1. Terms)
*  Response: The [PSS](1. Terms) accepts configuration with `ok 01`
*  If someone would try to configure a non-existing [Stacker](1. Terms) (for e.g with a 28mm [Pitch](1. Terms)), the response would be: `er 04` ([Wrong parameters in telegram](2.2 Error-Codes))


