# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from . import EnvironmentController_pb2 as EnvironmentController__pb2


class EnvironmentControllerStub(object):
  """Feature: Environment Controller
  Controls the incubation chamber environmental conditions e.g. CO2, H20, Temperature and O2 levels
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SetTargetTemperature = channel.unary_unary(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/SetTargetTemperature',
        request_serializer=EnvironmentController__pb2.SetTargetTemperature_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.SetTargetTemperature_Responses.FromString,
        )
    self.SetTargetCO2 = channel.unary_unary(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/SetTargetCO2',
        request_serializer=EnvironmentController__pb2.SetTargetCO2_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.SetTargetCO2_Responses.FromString,
        )
    self.Get_TargetTemperature = channel.unary_unary(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/Get_TargetTemperature',
        request_serializer=EnvironmentController__pb2.Get_TargetTemperature_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.Get_TargetTemperature_Responses.FromString,
        )
    self.Subscribe_CurrentTemperature = channel.unary_stream(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/Subscribe_CurrentTemperature',
        request_serializer=EnvironmentController__pb2.Subscribe_CurrentTemperature_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.Subscribe_CurrentTemperature_Responses.FromString,
        )
    self.Get_TargetCO2 = channel.unary_unary(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/Get_TargetCO2',
        request_serializer=EnvironmentController__pb2.Get_TargetCO2_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.Get_TargetCO2_Responses.FromString,
        )
    self.Subscribe_CurrentCO2 = channel.unary_stream(
        '/sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController/Subscribe_CurrentCO2',
        request_serializer=EnvironmentController__pb2.Subscribe_CurrentCO2_Parameters.SerializeToString,
        response_deserializer=EnvironmentController__pb2.Subscribe_CurrentCO2_Responses.FromString,
        )


class EnvironmentControllerServicer(object):
  """Feature: Environment Controller
  Controls the incubation chamber environmental conditions e.g. CO2, H20, Temperature and O2 levels
  """

  def SetTargetTemperature(self, request, context):
    """Set Target Temperature
    Sets the target temperature
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetTargetCO2(self, request, context):
    """Set Target CO2
    Sets the target CO2
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_TargetTemperature(self, request, context):
    """Target Temperature

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Subscribe_CurrentTemperature(self, request, context):
    """Current Temperature

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Get_TargetCO2(self, request, context):
    """Target CO2

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Subscribe_CurrentCO2(self, request, context):
    """Current Temperature

    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_EnvironmentControllerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SetTargetTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.SetTargetTemperature,
          request_deserializer=EnvironmentController__pb2.SetTargetTemperature_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.SetTargetTemperature_Responses.SerializeToString,
      ),
      'SetTargetCO2': grpc.unary_unary_rpc_method_handler(
          servicer.SetTargetCO2,
          request_deserializer=EnvironmentController__pb2.SetTargetCO2_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.SetTargetCO2_Responses.SerializeToString,
      ),
      'Get_TargetTemperature': grpc.unary_unary_rpc_method_handler(
          servicer.Get_TargetTemperature,
          request_deserializer=EnvironmentController__pb2.Get_TargetTemperature_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.Get_TargetTemperature_Responses.SerializeToString,
      ),
      'Subscribe_CurrentTemperature': grpc.unary_stream_rpc_method_handler(
          servicer.Subscribe_CurrentTemperature,
          request_deserializer=EnvironmentController__pb2.Subscribe_CurrentTemperature_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.Subscribe_CurrentTemperature_Responses.SerializeToString,
      ),
      'Get_TargetCO2': grpc.unary_unary_rpc_method_handler(
          servicer.Get_TargetCO2,
          request_deserializer=EnvironmentController__pb2.Get_TargetCO2_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.Get_TargetCO2_Responses.SerializeToString,
      ),
      'Subscribe_CurrentCO2': grpc.unary_stream_rpc_method_handler(
          servicer.Subscribe_CurrentCO2,
          request_deserializer=EnvironmentController__pb2.Subscribe_CurrentCO2_Parameters.FromString,
          response_serializer=EnvironmentController__pb2.Subscribe_CurrentCO2_Responses.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'sila2.de.unigoettingen.implementations.environmentcontroller.v1.EnvironmentController', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
