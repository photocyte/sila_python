sila\_python Implementations
============================

This is a collection of sample implementations. They can be used as
as source of inspiration for your own projects. They are not necessarily fully
elaborated/developed products, suited for production environments.
Please fee free to commit further projects to these folders.

Currently the Arduino based thermometers are fully functional (incl. serial interface connection).

Categories
-----------

-  analog\_digital\_converter
-  balances
-  barcode\_reader
-  camera\_video
-  centrifuges
-  container\_storage
-  dispensers
-  implementation\_concepts
-  incubators\_shakers
-  light\_detectors
-  liquid\_handlers
-  PCR
-  pumps
-  sila\_browser\_django
-  temperature\_controller
-  thermometers

New Ideas and Implementation Concepts
-------------------------------------

Proof-of-concept implementations and new ideas should be submitted in
the implemetation\_concepts folder.

Developers
-----------

Please select one of the category folders, if your project fits into and only
if there is nothing suited, create a new category folder, thanks. Refer
to the README file that each category contains, to see, if your project
fits.
