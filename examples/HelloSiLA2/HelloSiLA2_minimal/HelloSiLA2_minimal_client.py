#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelloSiLA2_Minimal client*

:details: HelloSiLA2_Minimal:
    This is Hello World SiLA2 test service

:file:    HelloSiLA2_Minimal_client.py
:authors: Timm Severin, Lukas Bromig

:date: (creation)          2020-03-12T16:17:51.512610
:date: (last modification) 2020-03-12T16:17:51.512610

.. note:: Code generated by sila2codegenerator 0.2.0

_______________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "1.0"

# import general packages
import logging
import argparse
import grpc
import time

# import meta packages
from typing import Union, Optional

# import SiLA2 library modules
from sila2lib.framework import SiLAFramework_pb2 as silaFW_pb2
from sila2lib.sila_client import SiLA2Client
from sila2lib.framework.std_features import SiLAService_pb2 as SiLAService_feature_pb2
from sila2lib.error_handling import client_err
#   Usually not needed, but - feel free to modify
# from sila2lib.framework.std_features import SimulationController_pb2 as SimController_feature_pb2

# import feature gRPC modules
# Import gRPC libraries of features
from GreetingProvider.gRPC import GreetingProvider_pb2
from GreetingProvider.gRPC import GreetingProvider_pb2_grpc
# import default arguments for this feature
from GreetingProvider.GreetingProvider_default_arguments import default_dict as GreetingProvider_default_dict


class HelloSiLA2_MinimalClient(SiLA2Client):
    """
        This is Hello World SiLA2 test service

    .. note:: For an example on how to construct the parameter or read the response(s) for command calls and properties,
              compare the default dictionary that is stored in the directory of the corresponding feature.
    """

    def __init__(self,
                 name: str = "HelloSiLA2_MinimalClient", description: str = "This is Hello World SiLA2 test service",
                 server_name: Optional[str] = None,
                 client_uuid: Optional[str] = None,
                 version: str = __version__,
                 vendor_url: str = "www.biovt.mw.tum.de",
                 server_hostname: str = "localhost", server_ip: str = "127.0.0.1", server_port: int = 50051,
                 cert_file: Optional[str] = None):
        """Class initialiser"""
        super().__init__(
            name=name, description=description,
            server_name=server_name,
            client_uuid=client_uuid,
            version=version,
            vendor_url=vendor_url,
            server_hostname=server_hostname, server_ip=server_ip, server_port=server_port,
            cert_file=cert_file
        )

        # Create stub objects used to communicate with the server
        self.GreetingProvider_stub = GreetingProvider_pb2_grpc.GreetingProviderStub(self.channel)

    def run(self) -> bool:
        """
        Starts the actual client and retrieves the meta-information from the server.

        :returns: True or False whether the connection to the server is established.
        """
        return True

    def stop(self, force: bool = False) -> bool:
        """
        Stop SiLA client routine
        :param force: If set True, the client is supposed to disconnect and stop immediately. Otherwise it can first try
                      to finish what it is doing.

        :returns: Whether the client could be stopped successfully or not.
        """
        return True

    def SayHello(self,
                      parameter: GreetingProvider_pb2.SayHello_Parameters = None) \
            -> GreetingProvider_pb2.SayHello_Responses:
        """
        Wrapper to call the unobservable command SayHello on the server.
    
        :param parameter: The parameter gRPC construct required for this command.
    
        :returns: A gRPC object with the response that has been defined for this command.
        """
        # noinspection PyUnusedLocal - type definition, just for convenience
        grpc_err: grpc.Call

        try:
            if parameter is None:
                parameter = GreetingProvider_pb2.SayHello_Parameters(Name=silaFW_pb2.String(value='John Cleese'))

            response = self.GreetingProvider_stub.SayHello(parameter)
            logging.debug('SayHello response: {response}'.format(response=response))
        except grpc.RpcError as grpc_err:
            self.grpc_error_handling(grpc_err)
            return None

        return response

    @staticmethod
    def grpc_error_handling(error_object: grpc.Call) -> None:
        """Handles exceptions of type grpc.RpcError"""
        # pass to the default error handling
        grpc_error =  client_err.grpc_error_handling(error_object=error_object)

if __name__ == '__main__':
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(format='%(levelname)-8s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    # start the server
    sila_client = HelloSiLA2_MinimalClient(server_ip='127.0.0.1', server_port=50051)

    logging.info('Running in Default-Mode = Simulation mode :')
    result = sila_client.SayHello(parameter=GreetingProvider_pb2.SayHello_Parameters(Name=silaFW_pb2.String(value='Simulans')))

    print(result.Greeting.value)
