"""
________________________________________________________________________

:PROJECT: SiLA2_python

*Greeting Provider*

:details: GreetingProvider:
    Minimum Feature Definition as example. Provides a Greeting

:file:    GreetingProvider_real.py
:authors: ben lear

:date: (creation)          2019-09-17T07:18:27.732757
:date: (last modification) 2019-09-17T07:18:27.732757

.. note:: Code generated by SiLA2CodeGenerator 0.2.0

________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.1"

# import general packages
import logging
import datetime
import grpc         # used for type hinting only

# import SiLA2 library
import sila2lib.framework.SiLAFramework_pb2 as silaFW_pb2

# import gRPC modules for this feature
from .gRPC import GreetingProvider_pb2 as GreetingProvider_pb2


class GreetingProviderReal:
    """
    Implementation of the *Greeting Provider* in *Real* mode
        This is Hello World SiLA2 test service
    """

    def __init__(self):
        """Class initialiser"""

        logging.debug('Started server in mode: {mode}'.format(mode='Real'))

    def SayHello(self, request, context: grpc.ServicerContext) \
            -> GreetingProvider_pb2.SayHello_Responses:
        """
        Executes the unobservable command "Say Hello"
            Does what it says
    
        :param request: gRPC request containing the parameters passed:
            request.Name (Name): Your Name
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: The return object defined for the command with the following fields:
            request.Greeting (Greeting): The greeting coming back at you
        """
    
        return GreetingProvider_pb2.SayHello_Responses(Greeting=silaFW_pb2.String(value=(
            'Hello {name}!'.format(name=request.Name.value)
        )))

    def Get_StartYear(self, request, context: grpc.ServicerContext) \
            -> GreetingProvider_pb2.Get_StartYear_Responses:
        """
        Requests the unobservable property Start Year
            The year the server has been started
    
        :param request: An empty gRPC request object (properties have no parameters)
        :param context: gRPC :class:`~grpc.ServicerContext` object providing gRPC-specific information
    
        :returns: A response object with the following fields:
            request.StartYear (Start Year): The year the server has been started
        """

        current_time = datetime.datetime.now()

        return GreetingProvider_pb2.Get_StartYear_Responses(StartYear=silaFW_pb2.Integer(value=current_time.year))
