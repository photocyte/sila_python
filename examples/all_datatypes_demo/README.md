 All datatypes Demo
======================

This demo shows the usage of all basic SiLA datatypes

To test this example, follow these steps:

  1. activate your virtual environment
  1. change to the demo directory:
       `cd all_datatypes_demo`
  1. start the server:
       `python3 HelloSiLA2_server.py`
  1. start the testclient in annother shell/shell:
       `python3 HelloSiLA2_testclient.py`
