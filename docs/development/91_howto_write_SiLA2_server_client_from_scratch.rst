Quickstart: how-to write a SiLA2 server / client from scratch
=============================================================

This is a very short Quickstart guide on how to write a SiLA server /
client from scratch

What do we need ?
=================

Features
--------

One of the core concepts of SiLA2 are features.

So take a sheet of paper and note down, which features you would like to
have.

A consitant way to store these features is the SiLA Feature description
language (FDL).

The scheme and some examples you can find in sila\_base

gRPC proto files
----------------

SiLA utilises the well supported and lean google Remote Procedure Call
(gRPC, [grpc.org] ) framework. It supports the most common programming
languages. Therefore you have the freedom to select the most appropiate
language to your skills/needs.

(s. )

The above designed Features shall be translated into gRPC proto files.
These are generic API description files for the gRPC framework.

This translation FDL -> gRPC proto files can be done manually or with
the help of condegenerators.

Currently there are code generators for Python, JAVA and C# available
(s. sila\_tools directory of the corresponding repository).

If you do that manually, please be aware of the namespace conventions
and parameter/response conventions

SiLAservice
-----------

zeroconfig / bonjour
--------------------
