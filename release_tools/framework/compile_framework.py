#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*compile_sila*

:details: This script compiles the ``.proto`` files in the sila_library 
          (``sila_library/sila2lib/framework/protobuf/*.proto``) to the corresponding python 
          ``_pb2.py`` and ``_pb2_grpc.py`` files. It should be executed after the ``.proto`` files 
          have been updated from the `sila\\_base <https://gitlab.com/SiLA2/sila_base>`_ repository.
           
:file:    compile_framework.py
:authors: Timm Severin

:date: (creation)          2019-08-19
:date: (last modification) 2019-08-19
________________________________________________________________________

"""

import os

import logging

from sila2lib.proto_builder.proto_compiler import compile_proto

# logging details
logging.basicConfig(format='%(levelname)s | %(module)s.%(funcName)s: %(message)s', level=logging.INFO)

# path to the directory of the proto files to compile
proto_path = os.path.join('..', '..', 'sila_library', 'sila2lib', 'framework', 'protobuf')

# target directory of generated stubs ( _pb2.py, _pb2_grpc.py files)
output_dir = os.path.join('..', '..', 'sila_library', 'sila2lib', 'framework')

logging.info('Reading all files from `{proto_path}`'.format(proto_path=proto_path))
logging.info('Writing output to `{output_dir}`'.format(output_dir=output_dir))

# get all proto files to compile
proto_files = [file for file in os.listdir(proto_path) if file[-6:] == '.proto']

# actually run the convert operation
for proto_file in proto_files:
    # use the libraries compiler function to compile the framework files
    result = compile_proto(proto_file=proto_file, source_dir=proto_path, target_dir=output_dir,
                           auto_include_library=False, import_local=True)
