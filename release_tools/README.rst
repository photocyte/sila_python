==============================
  SiLA\_python Release Tools
==============================

This folder contains a collection of tools used for the release cycle of the sila\_python repository on gitlab:

-------------
  framework
-------------

Contains scripts that can be used to work with the SiLA framework:

compile_framework.py
_____________________

  This script compiles the ``.proto`` files in the sila_library (``sila_library/sila2lib/framework/protobuf/*.proto``) to the corresponding python ``_pb2.py`` and ``_pb2_grpc.py`` files. It should be executed after the ``.proto`` files have been updated from the `sila\_base <https://gitlab.com/SiLA2/sila_base>`_ repository.

update_framework.py
____________________

  This script updates all framework files that are extracted from the `sila\_base <https://gitlab.com/SiLA2/sila_base>`_  repository. Therefore it creates a local clone of the repository and then copies the following files into the sila library:

  * Protocol buffer definitions: ``sila_base/protobuf/*.proto`` to ``sila2lib/framework/protobuf``
  * Schema files: ``sila_base/schema/*.xsd`` to ``sila2lib/framework/schema/``
  * Standard features: ``sila_base/feature_definitions/org/silastandard/core/*.xml`` to ``sila2lib/framework/feature_definitions/org.silastandard/``

  Files are only updated if a newer commit is available in the `sila\_base <https://gitlab.com/SiLA2/sila_base>`_ repository, and even then only if the files are different.

  The script will indicate which files have changed and in the end display how many files have changed.

  The current version of the framework files is identifier based on the sha commit hash of the `sila\_base <https://gitlab.com/SiLA2/sila_base>`_ repository, this has is stored in the sila library in the file `.sila_base_commit_hash </sila_library/sila2lib/framework/.sila_base_commit_hash>`_.

### install requirements
Please make sure, that all dependancies are installed:

.. code-block:: bash

  pip install -r requirements.txt
